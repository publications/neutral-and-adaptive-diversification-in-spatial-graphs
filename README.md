# Neutral and adaptive differentiation in spatial graphs

This repository contains scripts investigating neutral and adaptive differentiation in spatial graphs. The clean scripts used in Boussange et al (2022) can be found at an other git repository located [here](https://github.com/vboussange/differentiation-in-spatial-graphs).

- `code/` contains all scripts related to the simulation runs
    - `graph_utils` contains scripts to calculate graph properties
    - `simulations` contains the script to generate the simulations related to the paper settings
- `figure/` contains all scripts to create graphical output and crunch the raw simulation results

All scripts are written in the Julia programming language. You can download the software at https://julialang.org .
The scripts can be executed out of the box by activating the environment stored in the root folder (`code/Project.toml` file).
To activate the environment, type in the Julia REPL

```julia
julia>] activate relative/path/to/Project.toml_folder
```
or run 
```
> julia --project=relative/path/to/Project.toml_folder name_of_the_script.jl
```


#= Code to generate figure ??
Somehow there is a problem with colormap and with 

[cbox.spines[k].set_visible(false) for k in keys(cbox.spines)]

The plot is calculated for k = 1 and θ = 1/2
=#

cd(@__DIR__)
using LightGraphs
using DataFrames
using CSV
using PyPlot
using GLM
using Statistics
using JLD2
using EvoId
using Printf
using Random
using Polynomials
using DataStructures
using KernelDensity
include("../format.jl")
include("../../code/graphs_utils/src/graphs_utils.jl")

@load "../../code/simulations/setting_2/explo_r&m-pde/pde_meanfield_data.jld2" df_explo # data for heatmap

# we call an external scipt to set data in a nice dictionary
include("data_sett2_adapt_pde.jl")

#################
### PDE RESULT###
#################

fig,ax = subplots(2,1,figsize = (FIGSIZE_S[1], FIGSIZE_S[2] * 1.4),sharex = true)
ax2 = ax[1]
ax4 = ax[2]
cbbox1 = mplt.inset_axes(ax2,width = "20%", height = "40%",loc=4)
cbbox2 = mplt.inset_axes(ax4,width = "25%", height = "40%",loc=4)
for cbox in [cbbox1,cbbox2]
    cbox.tick_params(axis="both", left=false, top=false, right=false, bottom=false, labelleft=false, labeltop=false, labelright=false, labelbottom=false)
    # [cbox.spines[k].set_visible(false) for k in keys(cbox.spines)]
    # cbox.axis("off")
    cbox.set_facecolor([1,1,1,0.7])
end
cbar1 = mplt.inset_axes(cbbox1, "8%", "50%", loc = 4)
cbar2 = mplt.inset_axes(cbbox2, "8%", "50%", loc = 4)

pc = ax2.pcolormesh(pde_data["rθ"], pde_data["m"], pde_data["βs"], 
    # cmap = ColorMap(eth_grad_std.colors),
    # extent=[1,tend,S[1],S[end]]
    # vmax = K1
    # norm = matplotlib.colors.LogNorm()
    )
_cb = fig.colorbar(pc,cax =cbar1)
_cb.ax.set_xlabel(L"\beta_s",labelpad = 5)
_cb.ax.xaxis.set_label_position("top")
# _cb.ax.tick_params(axis="x",direction = "in", labeltop = true)
# cbar1.xaxis.set_label_position("top")
cbar1.yaxis.set_ticks_position("left")
# _cb.outline.set_edgecolor("white")
# _cb.outline.set_linewidth(2)

ax2.set_ylabel(L"m")
ax2.set_ylim(0.,1.)
# ax2.set_xlabel(L"r_\theta")

pc = ax4.pcolormesh(pde_data["rθ"],pde_data["m"], pde_data["N"] ./ 2 .* 7, 
    # cmap = ColorMap(eth_grad_std.colors),
    # extent=[1,tend,S[1],S[end]]
    # vmax = K1
    # norm = matplotlib.colors.LogNorm()
    )
gcf()
ax4.set_xlabel(L"r_\theta")
ax4.set_ylabel(L"m")

_cb = fig.colorbar(pc,cax = cbar2)
_cb.ax.set_xlabel(L"N",labelpad = 5)
_cb.ax.xaxis.set_label_position("top")
cbar2.yaxis.set_ticks_position("left")

###############
## drawing m* 
###############

x = pde_data["rθ"]
xs = -1:0.07:0.65
y = 1. ./ (1 .- x ) .* (1/(1 + 3/4))
ax2.plot(x,y,c = "red",label = L"m^*")
ax2.legend(bbox_to_anchor=(0.66, 1.), bbox_transform=ax2.transAxes)

_let = ["A","B"]
for (i,ax) in enumerate([ax2,ax4])
    _x = -0.2
    ax.text(_x, 1.0, _let[i],
        fontsize=12,
        fontweight="bold",
        va="bottom",
        ha="left",
        transform=ax.transAxes ,
    )
    ax.set_xticks(-1.:0.5:1.)
end

fig.tight_layout()


fig.savefig("pde_v3_horizontal.pdf",
            dpi=1200,
            # bbox_inches = "tight",
            )

gcf()

# fig.set_facecolor("None")
# ax1.set_facecolor("None")
# ax3.set_facecolor("None")
# ax3.set_facecolor("None")
# gcf()


# fig.savefig("slice_N_mu_$(m_toplot)_rtheta.png")

\begin{tabular}{lrrrr}
\toprule
               & \multicolumn{1}{c}{0.01} & \multicolumn{1}{c}{0.50} & \multicolumn{1}{c}{0.01} & \multicolumn{1}{c}{0.50} \\ 
\cmidrule(lr){2-2} \cmidrule(lr){3-3} \cmidrule(lr){4-4} \cmidrule(lr){5-5} 
               &            \multicolumn{2}{c}{$Q_{ST,s}$}           &            \multicolumn{2}{c}{$Q_{ST,u}$}           \\ 
\cmidrule(lr){2-3} \cmidrule(lr){4-5} 
(Intercept)    &                    0.000 &                    0.000 &                   -0.000 &                    0.000 \\ 
               &                  (0.008) &                  (0.011) &                  (0.010) &                  (0.010) \\ 
$h_d$          &                -0.101*** &                -0.111*** &                  -0.034* &                -0.114*** \\ 
               &                  (0.011) &                  (0.015) &                  (0.014) &                  (0.013) \\ 
$cl$           &                 0.079*** &                 0.453*** &                 0.595*** &                 0.449*** \\ 
               &                  (0.011) &                  (0.015) &                  (0.014) &                  (0.013) \\ 
$r_\theta$     &                 0.908*** &                 0.654*** &                -0.634*** &                 0.704*** \\ 
               &                  (0.008) &                  (0.011) &                  (0.010) &                  (0.010) \\ 
\midrule
Number of sim. &                    2,250 &                    2,250 &                    2,250 &                    2,250 \\ 
$R^2$          &                    0.854 &                    0.727 &                    0.768 &                    0.793 \\ 
\bottomrule
\end{tabular}

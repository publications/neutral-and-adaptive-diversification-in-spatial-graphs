\begin{tabular}{lrrrr}
\toprule
               & \multicolumn{1}{c}{0.01} & \multicolumn{1}{c}{0.50} & \multicolumn{1}{c}{0.01} & \multicolumn{1}{c}{0.50} \\ 
\cmidrule(lr){2-2} \cmidrule(lr){3-3} \cmidrule(lr){4-4} \cmidrule(lr){5-5} 
               &            \multicolumn{2}{c}{$Q_{ST,s}$}           &            \multicolumn{2}{c}{$Q_{ST,u}$}           \\ 
\cmidrule(lr){2-3} \cmidrule(lr){4-5} 
(Intercept)    &                    0.000 &                    0.000 &                   -0.000 &                    0.000 \\ 
               &                  (0.008) &                  (0.008) &                  (0.008) &                  (0.008) \\ 
$h_d$          &                -0.144*** &                -0.190*** &                -0.147*** &                -0.165*** \\ 
               &                  (0.011) &                  (0.011) &                  (0.011) &                  (0.011) \\ 
$cl$           &                   -0.019 &                 0.422*** &                 0.475*** &                 0.431*** \\ 
               &                  (0.011) &                  (0.011) &                  (0.011) &                  (0.011) \\ 
$r_\theta$     &                 0.917*** &                 0.720*** &                -0.730*** &                 0.727*** \\ 
               &                  (0.008) &                  (0.008) &                  (0.008) &                  (0.008) \\ 
\midrule
Number of sim. &                    2,250 &                    2,250 &                    2,250 &                    2,250 \\ 
$R^2$          &                    0.855 &                    0.854 &                    0.862 &                    0.852 \\ 
\bottomrule
\end{tabular}

\begin{tabular}{lrrrr}
\toprule
               & \multicolumn{1}{c}{0.01} & \multicolumn{1}{c}{0.50} & \multicolumn{1}{c}{0.01} & \multicolumn{1}{c}{0.50} \\ 
\cmidrule(lr){2-2} \cmidrule(lr){3-3} \cmidrule(lr){4-4} \cmidrule(lr){5-5} 
               &            \multicolumn{2}{c}{$Q_{ST,s}$}           &            \multicolumn{2}{c}{$Q_{ST,u}$}           \\ 
\cmidrule(lr){2-3} \cmidrule(lr){4-5} 
(Intercept)    &                   -0.000 &                   -0.000 &                    0.000 &                   -0.000 \\ 
               &                  (0.008) &                  (0.009) &                  (0.011) &                  (0.009) \\ 
$h_d$          &                -0.103*** &                -0.114*** &                   -0.024 &                -0.091*** \\ 
               &                  (0.009) &                  (0.010) &                  (0.013) &                  (0.010) \\ 
$cl$           &                    0.009 &                 0.310*** &                 0.400*** &                 0.297*** \\ 
               &                  (0.009) &                  (0.010) &                  (0.013) &                  (0.010) \\ 
$r_\theta$     &                 0.915*** &                 0.804*** &                -0.733*** &                 0.812*** \\ 
               &                  (0.008) &                  (0.009) &                  (0.011) &                  (0.009) \\ 
\midrule                                                                                                                                         
Number of sim. &                    2,548 &                    2,548 &                    2,548 &                    2,548 \\ 
$R^2$          &                    0.847 &                    0.807 &                    0.680 &                    0.800 \\ 
\bottomrule
\end{tabular}

#=
This script plots the last figure of the main text

habitat considered is forest

This is for now an unfruitful attempt 
to add coordinates in the goal of adding 
further lat long and country borders
to better situate the landscape
=#

# /!\ this script uses PyCall, that relies on python
# Make sure to install Python, as well as the packages
# "cartopy", "rasterio", "networkx", "matplotlib", "shapely".
# Then uncomment the two following lines, making sure to direct
# ENV["PYTHON"] your your python installation (see https://github.com/JuliaPy/PyCall.jl)
# for more explanations.
# Those two lines can be uncommented after the first usepackage

# ENV["PYTHON"] = "/usr/local/anaconda3/envs/land2graph/bin/python"
# using Pkg; Pkg.build("PyCall")

using PyCall
using Printf
using JLD2
ccrs = pyimport("cartopy.crs")
cf = pyimport("cartopy.feature")
rasterio = pyimport("rasterio")
using PyPlot
include("../../format.jl")
geometry = pyimport("shapely.geometry")
include("../../../code/graphs_utils/src/graphs_utils.jl")
cd(@__DIR__)
# this is needed for PyCall v1.92.5 
# cm_eth = ColorMap([c for c in eth_grad_std.colors])
# cmap = cm_eth
using ColorSchemes
cmap = ColorMap(ColorSchemes.tableau_red_blue.colors)
rivers_50m = cf.NaturalEarthFeature("physical", "rivers_lake_centerlines", "50m")
#################################
##### Reading habitat raster ####
#################################
filename_temp = "../../../code/graphs_utils/Land2Graph/data/CHELSA_bio1_reprojected_nearest.tif"
filename_hab = "../../../code/graphs_utils/Land2Graph/data/iucn_habitatclassification_fraction_lvl1__100_Forest__ver004.tif"
# loading rasters for (A) and (B)
data_src_temp = rasterio.open(filename_temp, "r")
data_src_hab = rasterio.open(filename_hab, "r")

xmin = 90.78596; xmax = 105.8718; ymin = 22.91373; ymax = 34.47213
# pixel coordinate for longitude / latitudes for top left corner
row, col = data_src_hab.index(xmin, ymax)
row2, col2 = data_src_hab.index(xmax, ymin)
# window_size = 10
area_threshold = 500

mytemp = data_src_temp.read(1, window = rasterio.windows.Window( col, row, col2-col, row2-row)) /10. .- 273.15 .|> Float64
myhab =  data_src_hab.read(1, window = rasterio.windows.Window( col, row, col2-col, row2-row)) .|> Float64

real_graph_df = load("../../../code/graphs_utils/real_graphs/realistic_graph_hengduans.jld2", "sub_df_interesting_graphs")

##########################################################
# small window within the considered region for fig. (A) #
##########################################################

# create figure
# define cartopy crs for the raster
crs = ccrs.PlateCarree()

# fig,axs = plt.subplots(1, 2,            
#                     figsize = FIGSIZE_L,
#                     subplot_kw = Dict("projection"=>crs))
# ax1,ax2 = axs

# create figure
fig = plt.figure(
            # constrained_layout=true,
            figsize = FIGSIZE_L
            )
gs = fig.add_gridspec(1, 2)
gs.update(wspace=0.3, hspace=0.) # set the spacing between axes.
ax1 = fig.add_subplot(py"$gs[0, 1]", projection=crs)
ax2 = fig.add_subplot(py"$gs[0, 0]", projection=crs)

################
#### ax1 #######
################
# ax1.set_title("Hengduan region")
ax1.set_xmargin(0.05)
ax1.set_ymargin(0.10)


################
#### (B) #######
################
# plot raster
pos1 = ax1.imshow(
                myhab/10,
                interpolation="nearest",
                cmap = :Greens,
                vmin = 0.0,
                vmax = 100.0,
                extent=[xmin, xmax, ymin, ymax],
                transform=crs,
                )
gl = ax1.gridlines(draw_labels=true)
gl.xlabels_top = false
gl.ylabels_right = false
gl.xlines = true
gl.ylines = true
gl.rotate_labels = false
gcf()
# plot features
ax1.coastlines(resolution="auto", color="red")
ax1.add_feature(cf.BORDERS, linestyle = "--", edgecolor = "grey")
ax1.add_feature(rivers_50m, facecolor="None", edgecolor="deepskyblue")
gcf()


# adding crosses on areas of the graph selected
# we add a cross on central pixel of the sub window
for r in eachrow(real_graph_df)
    # coordinates in the array
    y = col + r.geographic_coord[2]*r.window_size - (r.window_size/2-1)
    x = row + r.geographic_coord[1]*r.window_size - (r.window_size/2-1)
    # latitude / longitude
    lat, long = data_src_hab.xy(x, y)
    ax1.scatter(lat, long, c = "r", marker="+", transform=crs, zorder = 10)
end
ax1.axes.xaxis.set_visible(false)
ax1.axes.yaxis.set_visible(false)
gcf()
############################
############ (A) ###########
############################
ax2.set_xmargin(-0.01)
ax2.set_ymargin(-0.01)
[i.set_linewidth(3) for i in ax2.spines.values()]
[i.set_color("r") for i in ax2.spines.values()]

# plotting sub raster 
idx = 25 # choosing first graph considered {21,22,35}
wc = real_graph_df[idx,"geographic_coord"] # window coordinate (top left)
window_size = real_graph_df[idx,"window_size"]
xmin, ymax = data_src_hab.xy(row + wc[1]*window_size - (window_size-1), col + wc[2]*window_size - (window_size-1))
xmax, ymin = data_src_hab.xy(row + wc[1]*window_size + 1, col + wc[2]*window_size + 1) 
# notice that extent is given by coordinates of 0 and window_size + 1 pixel
pos2 = ax2.imshow(myhab[(wc[1]*window_size - (window_size-1)):(wc[1]*window_size), (wc[2]*window_size - (window_size-1)):(wc[2]*window_size)]/10,
                cmap = :Greens,
                origin="upper",
                vmin = 0.0,
                vmax = 100.0,
                extent=[xmin, xmax, ymin, ymax],
                transform=crs,
                )
gl = ax2.gridlines(draw_labels=true)
gl.xlabels_top = false
gl.ylabels_right = false
gl.xlines = false
gl.ylines = false
gl.rotate_labels = false
gcf()
# cb = colorbar(pos2, ax=ax2, label = "Grassland coverage", shrink=0.5)
# vals = cb.ax.get_yticks()
# cb.ax.set_yticklabels([@sprintf("%15.0f", x)*L"\%" for x in vals])

# Plotting graph on top
gx = to_nx(real_graph_df[idx, "graphs"])
coord_graph = real_graph_df[idx,"pos_interesting_graph_unorganised"]
temp = real_graph_df[idx, "temps"]
# assiging coordinates to the graph nodes
pos = Dict{Int,Array}()
row2 = row + wc[1]*window_size - (window_size-1)
col2 = col + wc[2]*window_size - (window_size-1)
for i in keys(coord_graph)
    xs, ys = data_src_hab.xy(row2 + coord_graph[i][2] + 0.5, col2 + coord_graph[i][1] + 0.5, offset="center")
    pos[i] = [xs[], ys[]]
end
nx.draw_networkx_nodes(gx,
                        pos,
                        node_size = 25.,
                        node_color = temp,
                        cmap=cmap,
                        ax = ax2,
                        )
nx.draw_networkx_edges(gx, pos, alpha=0.9, width=1,ax=ax2)
gcf()



# colorbar grassland
# fig.subplots_adjust(right=0.8)
cbar_ax = fig.add_axes([0.96, 0.55, 0.02, 0.1])
cb = fig.colorbar(pos1, cax=cbar_ax, label = "Grassland coverage",shrink=10)
vals = cb.ax.get_yticks()
cb.ax.set_yticklabels([@sprintf("%15.0f", x)*L"\%" for x in vals])

## Colorbar vertices
# colorbar grassland
cbar_ax2 = fig.add_axes([0.96, 0.35, 0.02, 0.1])
sm = matplotlib.cm.ScalarMappable(cmap=cmap, norm=matplotlib.colors.Normalize(vmin=minimum(temp), vmax=maximum(temp)))
cb = fig.colorbar(sm, cax=cbar_ax2, label = "Std. temperature")
# vals = cb.ax.get_yticks()
# cb.ax.set_yticklabels([@sprintf("%15.0f", x)*L"\%" for x in vals])


## draw nice lines between (B) and (A)
x = row + wc[1]*window_size - (window_size-1)
y = col + wc[2]*window_size - (window_size-1)
xyB = data_src_hab.xy(x, y)
xyA = (1, 1)
con = matplotlib.patches.ConnectionPatch(
    xyA=xyA,
    xyB=xyB,
    coordsA=ax2.transAxes,
    coordsB="data",
    axesA=ax2,
    axesB=ax1,
    color = "r")
ax2.add_artist(con)
# low
xyA = (1, 0)
con = matplotlib.patches.ConnectionPatch(
    xyA=xyA,
    xyB=xyB,
    coordsA=ax2.transAxes,
    coordsB="data",
    axesA=ax2,
    axesB=ax1,
    color = "r")
ax2.add_artist(con)

fig.savefig("land2graphs.pdf",
            dpi=1200,
            bbox_inches = "tight",
            )

gcf()
\begin{tabular}{lrrrr}
\toprule
               & \multicolumn{1}{c}{0.01} & \multicolumn{1}{c}{0.50} & \multicolumn{1}{c}{0.01} & \multicolumn{1}{c}{0.50} \\ 
\cmidrule(lr){2-2} \cmidrule(lr){3-3} \cmidrule(lr){4-4} \cmidrule(lr){5-5} 
               &            \multicolumn{2}{c}{$Q_{ST,s}$}           &            \multicolumn{2}{c}{$Q_{ST,u}$}           \\ 
\cmidrule(lr){2-3} \cmidrule(lr){4-5} 
(Intercept)    &                    0.000 &                   -0.000 &                   -0.000 &                   -0.000 \\ 
               &                  (0.007) &                  (0.008) &                  (0.011) &                  (0.007) \\ 
$h_d$          &                -0.099*** &                -0.131*** &                 -0.040** &                -0.124*** \\ 
               &                  (0.008) &                  (0.009) &                  (0.012) &                  (0.008) \\ 
$cl$           &                   -0.001 &                 0.296*** &                 0.370*** &                 0.278*** \\ 
               &                  (0.008) &                  (0.009) &                  (0.012) &                  (0.008) \\ 
$r_\theta$     &                 0.936*** &                 0.814*** &                -0.755*** &                 0.844*** \\ 
               &                  (0.007) &                  (0.008) &                  (0.011) &                  (0.007) \\ 
\midrule
Number of sim. &                    2,548 &                    2,548 &                    2,548 &                    2,548 \\ 
$R^2$          &                    0.885 &                    0.826 &                    0.695 &                    0.858 \\ 
\bottomrule
\end{tabular}

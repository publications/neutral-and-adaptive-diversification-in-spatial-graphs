\begin{tabular}{lrrrr}
\toprule
               & \multicolumn{1}{c}{0.01} & \multicolumn{1}{c}{0.50} & \multicolumn{1}{c}{0.01} & \multicolumn{1}{c}{0.50} \\ 
\cmidrule(lr){2-2} \cmidrule(lr){3-3} \cmidrule(lr){4-4} \cmidrule(lr){5-5} 
               &            \multicolumn{2}{c}{$Q_{ST,s}$}           &            \multicolumn{2}{c}{$Q_{ST,u}$}           \\ 
\cmidrule(lr){2-3} \cmidrule(lr){4-5} 
(Intercept)    &                   -0.000 &                   -0.000 &                   -0.000 &                   -0.000 \\ 
               &                  (0.093) &                  (0.063) &                  (0.056) &                  (0.059) \\ 
$h_d$          &                   -0.087 &                -0.235*** &                   -0.048 &                -0.286*** \\ 
               &                  (0.094) &                  (0.064) &                  (0.057) &                  (0.060) \\ 
$cl$           &                  -0.218* &                  0.199** &                 0.965*** &                 0.645*** \\ 
               &                  (0.106) &                  (0.073) &                  (0.064) &                  (0.068) \\ 
$r_\theta$     &                 0.609*** &                 0.675*** &                -0.282*** &                 0.282*** \\ 
               &                  (0.106) &                  (0.072) &                  (0.063) &                  (0.068) \\ 
\midrule
Number of sim. &                       83 &                       83 &                       83 &                       83 \\ 
$R^2$          &                    0.311 &                    0.678 &                    0.752 &                    0.717 \\ 
\bottomrule
\end{tabular}

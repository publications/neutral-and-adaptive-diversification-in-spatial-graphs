#=
Transient plots for stabilising selection
=#
using Random
cd(@__DIR__)
using Dates
using JLD2
using Revise
using EvoId,LightGraphs,UnPack
using LaTeXStrings, Printf
using IDEvol
using PyPlot
include("../../format.jl")
Line2D = matplotlib.lines.Line2D

# name_scenario = "transients_adapt_weekly_stab_selec"
# name_scenario = "transients_adapt"
# name_scenario = "transients_adapt_m01-05"
name_scenario = "transients_adapt_nodes_7"
logsc = false
@load "$(name_scenario).jld2" df dim_neutr nodes p_neutr
###############
###plotting####
###############
# tstepend = 500
tstart = 1 
# tsteps_log = unique(floor.(exp.(range(log(1),log(500),length=100)))) .|> Int # to discard visual effect when plotting on log xscale 
plt.clf()
fig, axs = plt.subplots(1,3,figsize=(FIGSIZE_M[1]*2,FIGSIZE_M[2]*0.8))
dfg = groupby(df,"graph")
for _df in dfg
    for r in eachrow(_df)
            s = r.simulation
            axs[1].plot(s.tspan[tstart:end],s["betau"][tstart:end] ./ (s["betau"][tstart:end] .+ s["alphau"][tstart:end]),
                        label =  (r.graph == star_graph(nodes) ? "star graph" : "complete graph")*", m = $(@sprintf("%.2f",r.m))",
                        # c =  (r.m < 0.3 ? "tab:blue" : "tab:red"),
                        alpha=0.7,
                        linestyle = r.graph == star_graph(nodes) ? ":" : "-"
                        )
            idx_time = 1901 .< s.tspan .< 2000
            Q_ST_u = mean(s["betau"][idx_time] ./ (s["betau"][idx_time] .+ s["alphau"][idx_time]))
            axs[1].hlines(Q_ST_u, 0., 2000., linestyle = r.graph == star_graph(nodes) ? ":" : "-",color="r")
            idx_time = 901 .< s.tspan .< 1000
            Q_ST_u = mean(s["betau"][idx_time] ./ (s["betau"][idx_time] .+ s["alphau"][idx_time]))
            axs[1].hlines(Q_ST_u, 0., 2000., linestyle = r.graph == star_graph(nodes) ? ":" : "-",color="g")

            logsc ? axs[1].set_yscale("log") : nothing
            
            axs[2].plot(s.tspan[tstart:end],s["betas"][tstart:end] ./ (s["alphas"][tstart:end] .+ s["betas"][tstart:end]),
                        # c =  (r.m < 0.3 ? "tab:blue" : "tab:red"),
                        linestyle = r.graph == star_graph(nodes) ? ":" : "-",
                        alpha=0.7
                        );
            Q_ST_s = mean(s["betas"][idx_time] ./ (s["betas"][idx_time] .+ s["alphas"][idx_time]))
            axs[2].hlines(Q_ST_s, 0., 2000., linestyle = r.graph == star_graph(nodes) ? ":" : "-",)

            logsc ? axs[2].set_yscale("log") : nothing
            # axs[2].set_ylabel( L"Q_{ST,u}");axs[1].set_yscale("log")
            # axs[2].set_ylim(0,1.0)

            axs[3].plot(s.tspan[tstart:end],s["N"][tstart:end],
                        # c =  (r.m < 0.3 ? "tab:blue" : "tab:red"),
                        linestyle = r.graph == star_graph(nodes) ? ":" : "-",
                        alpha=0.7
                        ); 
            # axs[3].set_yscale("log")
            axs[1].set_ylabel( L"Q_{ST,u}");
            axs[2].set_ylabel( L"Q_{ST,s}");
            axs[3].set_ylabel( L"N");
    end
end
[ax.set_xlabel("Time") for ax in axs]
# [ax.set_xscale("log") for ax in axs]
[ax.set_ylim(ax.get_ylim()...) for ax in axs]
[ax.vlines(500., ax.get_ylim()...) for ax in axs]

# custom legend
# fig.legend(handles=[Line2D([0], [0], color="grey", linestyle=":", label="star graph"),
#             Line2D([0], [0], color="grey", label="complete graph")],loc="upper left") #colors
# fig.legend(handles=[Line2D([0], [0], color="tab:blue", linestyle=":", label="m = 0.1"),
#                     Line2D([0], [0], color="tab:red", label="m=0.3")],loc="upper right") #linestyle
# other legend style
fig.legend(bbox_to_anchor=[1.0,1.4], bbox_transform = axs[2].transAxes)
gcf()

_let = ["a","b","c"]
_offx = - 0.2
_offy =  1.05
for (i,ax) in enumerate(axs)
    ax.text(_offx, _offy, _let[i],
        fontsize=12,
        fontweight="bold",
        va="bottom",
        ha="left",
        transform=ax.transAxes ,
    )
end

fig.tight_layout()

fig.savefig("transient_setting2_dim_neutr=$(dim_neutr)_$(name_scenario)_nodes_$(nodes)_log_$(string(logsc))_p_neutr_$(p_neutr).pdf",
            dpi=1200,
            bbox_inches = "tight",
            )

gcf()

# Plotting beta_u

plt.clf()
fig, axs = plt.subplots(1,3,figsize=(FIGSIZE_M[1]*2,FIGSIZE_M[2]*0.8))
dfg = groupby(df,"graph")
for _df in dfg
    for r in eachrow(_df)
            s = r.simulation
            axs[1].plot(s.tspan[tstart:end],s["betau"][tstart:end],
                        label =  (r.graph == star_graph(nodes) ? "star graph" : "complete graph")*", m = $(@sprintf("%.2f",r.m))",
                        # c =  (r.m < 0.3 ? "tab:blue" : "tab:red"),
                        alpha=0.7,
                        linestyle = r.graph == star_graph(nodes) ? ":" : "-"
                        )
            idx_time = 1901 .< s.tspan .< 2000
            Q_ST_u = mean(s["betau"][idx_time])
            axs[1].hlines(Q_ST_u, 0., 2000., linestyle = r.graph == star_graph(nodes) ? ":" : "-",color="r")
            idx_time = 901 .< s.tspan .< 1000
            Q_ST_u = mean(s["betau"][idx_time])
            axs[1].hlines(Q_ST_u, 0., 2000., linestyle = r.graph == star_graph(nodes) ? ":" : "-",color="g")

            logsc ? axs[1].set_yscale("log") : nothing
            
            axs[2].plot(s.tspan[tstart:end],s["betas"][tstart:end],
                        # c =  (r.m < 0.3 ? "tab:blue" : "tab:red"),
                        linestyle = r.graph == star_graph(nodes) ? ":" : "-",
                        alpha=0.7
                        );
            Q_ST_s = mean(s["betas"][idx_time] )
            axs[2].hlines(Q_ST_s, 0., 2000., linestyle = r.graph == star_graph(nodes) ? ":" : "-",)

            logsc ? axs[2].set_yscale("log") : nothing
            # axs[2].set_ylabel( L"Q_{ST,u}");axs[1].set_yscale("log")
            # axs[2].set_ylim(0,1.0)

            axs[3].plot(s.tspan[tstart:end],s["N"][tstart:end],
                        # c =  (r.m < 0.3 ? "tab:blue" : "tab:red"),
                        linestyle = r.graph == star_graph(nodes) ? ":" : "-",
                        alpha=0.7
                        ); 
            # axs[3].set_yscale("log")
            axs[1].set_ylabel( L"\beta_u");
            axs[2].set_ylabel( L"\beta_s");
            axs[3].set_ylabel( L"N");
    end
end
[ax.set_xlabel("Time") for ax in axs]
# [ax.set_xscale("log") for ax in axs]
[ax.set_ylim(ax.get_ylim()...) for ax in axs]
[ax.vlines(500., ax.get_ylim()...) for ax in axs]

# custom legend
# fig.legend(handles=[Line2D([0], [0], color="grey", linestyle=":", label="star graph"),
#             Line2D([0], [0], color="grey", label="complete graph")],loc="upper left") #colors
# fig.legend(handles=[Line2D([0], [0], color="tab:blue", linestyle=":", label="m = 0.1"),
#                     Line2D([0], [0], color="tab:red", label="m=0.3")],loc="upper right") #linestyle
# other legend style
fig.legend(bbox_to_anchor=[1.0,1.4], bbox_transform = axs[2].transAxes)
gcf()

_let = ["a","b","c"]
_offx = - 0.2
_offy =  1.05
for (i,ax) in enumerate(axs)
    ax.text(_offx, _offy, _let[i],
        fontsize=12,
        fontweight="bold",
        va="bottom",
        ha="left",
        transform=ax.transAxes ,
    )
end

fig.tight_layout()

fig.savefig("transient_setting2_dim_neutr=$(dim_neutr)_$(name_scenario)_nodes_$(nodes)_log_$(string(logsc))_betau_p_neutr_$(p_neutr).pdf",
            dpi=1200,
            bbox_inches = "tight",
            )

gcf()


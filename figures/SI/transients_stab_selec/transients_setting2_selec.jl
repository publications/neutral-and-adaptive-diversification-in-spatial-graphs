#=
transient simulations with stabilising selection

=# 
using Random
cd(@__DIR__)
using Dates
using JLD2
using EvoId,LightGraphs,UnPack
using ProgressMeter

######################
## global param def ##
######################

Tf = Float32
nodes = 7
dim_neutr = 1f0 # Float32 because used in the birth function
p_neutr = 1f0 # selection strength
μ = 1f-1
ms = Float32[1e-1, 5e-1]
adaptivespace = RealSpace{1,Tf}()
neutralspace = RealSpace{Int(dim_neutr),Tf}()
const K1 = 150f0;
@inbounds d(X, Y, t) = (X[1][] ≈ Y[1][]) ? 1f0 / K1 : 0f0
NMax = 2000
# tend = Tf(1.5)
tend = 2000.
D2 = Tf(5e-2)
# D = [nothing, D2, fill(D2,Int(dim_neutr))] # for dim_neutr > 1
D = [nothing, D2, D2]

t_saving_cb = collect(range(1, tend, length=8000)) #normal stepping
# t_saving_cb = collect(exp.(range(log(0.01), log(tend), length=500))) #log stepping

function cb(w)
        Dict(
                "alphas" => get_alpha_div(w,2),
                "betas" => get_beta_div(w,2),
                # "gammas" => mean(var(w,trait=2)),
                "alphau" => get_alpha_div(w,3),
                "betau" => get_beta_div(w,3),
                # "gammau" => mean(var(w,trait=3)),
                "N" => length(w))
end

p_default = Dict{String,Any}();@pack! p_default = NMax,D


pars = []
totgraph = 0
for _m in ms
        for g in [star_graph(nodes), complete_graph(nodes)] #
                soptim = 0.5f0 * Float32[-1,1,-1,1,-1,1,-1,1,-1]
                # mu = [Tf(_m),Tf(μ),fill(Tf(μ),Int(dim_neutr))]  # for dim_neutr > 1
                mu = [Tf(_m),Tf(μ),Tf(μ)]        

                geospace = GraphSpace(g)
                myspace = (geospace,adaptivespace,neutralspace)
                # b(X,t) = 1f0 - (X[2][] - soptim[Int(X[1][])])^2 - sum(X[1].^2) / dim_neutr # for dim_neutr > 1
                b(X,t) = 1f0 - (X[2][] - soptim[Int(X[1][])])^2 - p_neutr * sum(X[3].^2) 
                p = copy(p_default)
                @pack! p = b, d, myspace, mu, soptim
                push!(pars,p)
        end
end
function sim(p)
        @unpack b, d, myspace, D, mu, NMax, soptim = p
        myagents = [Agent(myspace, [[xpos], [D[2] * randn(Tf)],
                                        D[3] .* randn(Tf,Int(dim_neutr))]) for xpos in rand(1:nodes,nodes*Int(K1))]
        w0 = World(myagents, myspace, D, mu, NMax, 0.)
        # try
                s = run!(w0,Gillepsie(), tend, b, d,cb=cb, t_saving_cb = copy(t_saving_cb));
                return (mu[1][1], myspace[1].g, s)
        # catch e
        #         println("problem with p = $p")
        #         println(e)
        #         return nothing
        # end
end
    
#########################
## running experiments ##
#########################
progr = Progress(length(pars))
df = DataFrame("m" => zeros(length(pars)), "graph" => Vector{SimpleGraph}(undef,length(pars)), "simulation" => Vector{EvoId.Simulation}(undef,length(pars)))
@Threads.threads for k in 1:length(pars)
    df[k,:] = sim(pars[k]);
    next!(progr)
end

@save "transients_adapt_nodes_$nodes.jld2" df dim_neutr nodes p_neutr
#=
Script to plot ODE sol vs PDE sol in the setting 1

=#

cd(@__DIR__)
name_sim = split(splitpath(@__FILE__)[end],".")[1]
# name_sim =  "explo_G_2D_discrete_D2_1e-2"
using DiffEqOperators,LinearAlgebra,LightGraphs,Distributions
using DifferentialEquations,Random
using Plots,Printf
using UnPack,DataFrames,JLD2,Dates
import EvoId:gaussian
using IDEvol
using PyPlot
include("../../format.jl")

df_explo_ODE = load("explo_m_ODE/explo_m_ODE_2022-03-08.jld2", "df_explo")

df_explo_PDE = load("explo_m_PDE_setting1/explo_m_PDE_setting1_2022-03-08.jld2", "df_explo")

fig,ax = subplots(1,2,figsize = (FIGSIZE_S[2] * 2.1, FIGSIZE_S[1]), sharex = true)

# Q_TS_u diversity
ax[1].plot(df_explo_PDE.m,
        df_explo_PDE.βdiv ./ (df_explo_PDE.αdiv .+ df_explo_PDE.βdiv),
        # c = mapper.to_rgba(log10(_m)),
        label = "PDE results",
        # fmt = "o",
        ms = 4.)
ax[1].plot(df_explo_ODE.m,
        df_explo_ODE.βdiv ./ (df_explo_ODE.αdiv .+ df_explo_PDE.βdiv),
        label = "ODE result",
        # yerr = df_toplot_var[:,:betas],
        # c = mapper.to_rgba(log10(_m)),
        # label = "m = "*(@sprintf "%2.2f" _m),
        # fmt = "o",
        ms = 4.)
ax[1].set_xlabel(L"m"); ax[1].set_ylabel(L"Q_{ST,u}");
gcf()

# N_Mean diversity
ax[2].plot(df_explo_PDE.m,
        df_explo_PDE.popsize,
        # c = mapper.to_rgba(log10(_m)),
        # label = "IBM simulations",
        ms = 4.)
ax[2].plot(df_explo_ODE.m,
        df_explo_ODE.popsize .* (1. - 0.5 * 5e-2 * sqrt(0.1)) ,
        # label = "PDE result",
        # yerr = df_toplot_var[:,:betas],
        # c = mapper.to_rgba(log10(_m)),
        # label = "m = "*(@sprintf "%2.2f" _m),
        # fmt = "o",
        ms = 4.)
ax[2].set_xlabel(L"m"); ax[2].set_ylabel(L"N");
fig.legend(loc="upper center", bbox_to_anchor=(0.5, 1.1),
          ncol=3,
          fancybox=true,
          # shadow=true
          )
# [_ax.set_xscale("log") for _ax in ax]
fig.tight_layout()
fig.savefig("pde-vs-IBM-mresponse-setting1.pdf",
        dpi=1200,
        bbox_inches = "tight",)
gcf()

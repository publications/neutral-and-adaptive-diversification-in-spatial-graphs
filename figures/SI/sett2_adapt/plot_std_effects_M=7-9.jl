cd(@__DIR__)
using EvoId,JLD2,FileIO
using DataFrames
using Printf;#pyplot()
using Glob
using LightGraphs
using LaTeXStrings
using KernelDensity
using GLM, Interpolations, Polynomials
using Plots:ColorGradient
using ColorSchemes
using PyPlot
cm_eth = ColorMap([c for c in eth_grad_std.colors]);
include("../../../code/graphs_utils/src/graphs_utils.jl")
using RegressionTables
# isdir("img") ? nothing : mkdir("img")
## graphs prop

include("../../format.jl")

using CSV
# data figure C
fig,axs = subplots(1,2,figsize = (FIGSIZE_S[2] * 2.1, FIGSIZE_S[1]))

# dates of the simulations to be used
dates = ["2022-01-13","2022-01-14"]

for (i,M) in enumerate([7,9])
    @show i
    ax = axs[i]
    @load "../../../code/simulations/setting_2/M=$M/setting_2_mu_01_M=$(M)_hetero_2_[-onehalf,onehalf]/setting_2_mu_01_M=$(M)_hetero_2_[-onehalf,onehalf]_$(dates[i])_aggreg.jld2" df_aggreg
    df_aggreg_g = groupby(df_aggreg,:m,sort=true)


    # calculating GLM for each m and extracting coefficients
    sqrtk_coeff = []; sqrtk_coeff_err = []; cl_coeff = []; cl_coeff_err = []; rθ_coeff = []; rθ_coeff_err = []; lms = [];
    for i in 1:length(df_aggreg_g)
        df_temp = DataFrame(df_aggreg_g[i][:,["Q_ST_s_mean","sqrtk", "cl", "rθ"]]); [df_temp[!,n] = _scale(df_temp[:,n]) .|> Float64 for n in names(df_temp)]
        mylm = lm(@formula(Q_ST_s_mean ~ sqrtk + cl + rθ), df_temp)
        push!(lms,mylm)
        sqrtk = coeftable(mylm).cols[1][2]
        push!(sqrtk_coeff,sqrtk);
        push!(sqrtk_coeff_err,[sqrtk - coeftable(mylm).cols[5][2],coeftable(mylm).cols[6][2] - sqrtk]);
        cl = coeftable(mylm).cols[1][3]
        push!(cl_coeff,cl)
        push!(cl_coeff_err,[cl - coeftable(mylm).cols[5][3],coeftable(mylm).cols[6][3] - cl]);
        rθ = coeftable(mylm).cols[1][4]
        push!(rθ_coeff,rθ)
        push!(rθ_coeff_err,[rθ - coeftable(mylm).cols[5][4],coeftable(mylm).cols[6][4] - rθ]);
    end
    ms = unique(df_aggreg.m)
    cols = ["tab:blue", "tab:orange", "tab:red"]
    ax.errorbar(ms,sqrtk_coeff, yerr = hcat(sqrtk_coeff_err...), label = L"h_d", capsize = 2., capthick = 1., c = cols[1])
    ax.errorbar(ms,cl_coeff, yerr = hcat(cl_coeff_err...), label = L"cl", capsize = 2., capthick = 1., c = cols[2])
    ax.errorbar(ms,rθ_coeff, yerr = hcat(rθ_coeff_err...), label = L"r_\theta", capsize = 2., capthick = 1., c = cols[3])
    ax.hlines(0., 1e-2, 1e0, colors="grey", linestyles = "--", label = "")

    rθ = -1. # rθ
    p = 1; θ=0.5
    mstar = 1. ./ (1 .- rθ ) .* 4 * p * θ^2/(1 + 3*p * θ^2) #critical threshold
    @show mstar
    ax.axvline(mstar,label=L"m^*",linestyle="--" )

    ax.legend()
    ax.set_xscale("log")
    ax.set_ylabel("Standardized effect")
    ax.set_xlabel(L"m")
    ax.set_title("M = $M")
    ax.set_facecolor("None")

    ###########################
    #### printing latex table #
    ###########################
    regtable(lms...; renderSettings = latexOutput("setting2_adapt_M=$(M).txt"),
            print_estimator_section=false,
            regression_statistics=[:r2],
            labels = Dict("betau_mean" => "\\beta_u",
                            "sqrtk"=> "h_d",
                            "rθ"=> "r_\\theta",
                            "Q_ST_s_mean" => "Q_{ST,s}"),
            groups = [@sprintf("%1.2f",df.m[1]) for df in df_aggreg_g],
            number_regressions = false)
end

fig.set_facecolor("None")
###################
### annotating ####
##################
fig.tight_layout()
fig.savefig("setting2_adapt_M=7-9.pdf",
            dpi=1200,
            bbox_inches = "tight")
gcf()


#=

This script aims at understanding the discrepancies acros different simulation times
in the setting 2


scatter plot are mostly used

alphau, betau, q_st_u against r_\theta

=#

cd(@__DIR__)
using EvoId,JLD2,FileIO
using DataFrames
using Printf;#pyplot()
using Glob
using LightGraphs
using LaTeXStrings
using KernelDensity
using GLM, Interpolations, Polynomials
using Plots:ColorGradient
using ColorSchemes
using PyPlot
cm_eth = ColorMap([c for c in eth_grad_std.colors]);
include("../../../code/graphs_utils/src/graphs_utils.jl")
# isdir("img") ? nothing : mkdir("img")
## graphs prop

include("../../format.jl")
M = 7
df_aggreg_t1000_set1 = load("../../../code/simulations/setting_1/M=$M/setting_1_mu_01_M=$(M)/setting_1_mu_01_M=$(M)_2022-01-09_aggreg.jld2", "df_aggreg")
df_aggreg_t2000_set1 = load("../../../code/simulations/setting_1/M=$M/setting_1_mu_01_M=$(M)/setting_1_mu_01_M=$(M)_2022-02-16_aggreg.jld2", "df_aggreg")
df_aggreg_t1000_set2 = load("../../../code/simulations/setting_2/M=$M/setting_2_mu_01_M=7_hetero_2_[-onehalf,onehalf]/setting_2_mu_01_M=7_hetero_2_[-onehalf,onehalf]_2022-01-13_aggreg.jld2", "df_aggreg")
df_aggreg_t2000_set2 = load("../../../code/simulations/setting_2/M=$M/setting_2_mu_01_M=7_hetero_2_[-onehalf,onehalf]/setting_2_mu_01_M=7_hetero_2_[-onehalf,onehalf]_2022-02-18_aggreg.jld2", "df_aggreg")
df_aggreg_t3000_set2 = load("../../../code/simulations/setting_2/M=$M/setting_2_mu_01_M=7_hetero_2_[-onehalf,onehalf]/setting_2_mu_01_M=7_hetero_2_[-onehalf,onehalf]_2022-02-26_aggreg.jld2", "df_aggreg")

# checking Q_ST_u residuals wrt N_mean
#= conclusion

r_\theta does indeed explain some of Q_ST variability in all regimes
but mostly at low migration (0.01) and high migration regimes (1.0)

Its effect decreases drastically for m > 0.05, although indeed the direction changes
=#
idx_m = 1
df_aggreg_g = groupby(df_aggreg_t1000_set2,:m,sort=true)
df_temp = DataFrame(df_aggreg_g[idx_m][:,["Q_ST_u_mean","m","betau_mean","N_mean", "sqrtk","cl", "rθ"]]); [df_temp[!,n] = _scale(df_temp[:,n]) .|> Float64 for n in names(df_temp)]
mylmres = lm(@formula(Q_ST_u_mean ~ N_mean), df_temp)
df_temp[!,"Q_ST_u_residuals"] = residuals(mylmres)
mylm = lm(@formula(Q_ST_u_residuals ~ sqrtk + cl + rθ), df_temp)
println(mylm)
println(r2(mylm))

# checking betau residuals wrt N_mean
#= conclusion

we find similar conclusions as above
=#
idx_m = 6
df_aggreg_g = groupby(df_aggreg_t1000_set2,:m,sort=true)
df_temp = DataFrame(df_aggreg_g[idx_m][:,["Q_ST_u_mean","m","betau_mean","N_mean", "sqrtk","cl", "rθ"]]); [df_temp[!,n] = _scale(df_temp[:,n]) .|> Float64 for n in names(df_temp)]
mylmres = lm(@formula(betau_mean ~ N_mean), df_temp)
df_temp[!,"betau_residuals"] = residuals(mylmres)
mylm = lm(@formula(betau_residuals ~ sqrtk + cl + rθ), df_temp)
println(mylm)
println(r2(mylm))

# betau Q_ST_u
#= conclusion

we find similar conclusions as above
=#
idx_m = 1
fig,axs = subplots(3,2,figsize = (FIGSIZE_S[2] * 2.1, 3*FIGSIZE_S[1]))
# t = 3000
df_aggreg_g = groupby(df_aggreg_t3000_set2,:m,sort=true)
axs[1,1].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].betau_mean)
axs[1,1].set_ylabel("betau")
axs[1,2].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].Q_ST_u_mean)
axs[1,2].set_ylabel("Q ST u mean")
# t = 2000
df_aggreg_g = groupby(df_aggreg_t2000_set2,:m,sort=true)
axs[2,1].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].betau_mean)
axs[2,1].set_ylabel("betau")
axs[2,2].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].Q_ST_u_mean)
axs[2,2].set_ylabel("Q ST u mean")
# t = 1000
df_aggreg_g = groupby(df_aggreg_t1000_set2,:m,sort=true)
axs[3,1].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].betau_mean)
axs[3,1].set_ylabel("betau")
axs[3,2].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].Q_ST_u_mean)
axs[3,2].set_ylabel("Q ST u mean")
fig.suptitle("m = $(df_aggreg_g[idx_m].m[1])")
gcf()


## testing alphau, betau, alphau + betau, Q_ST_u
#= conclusions

We find that alphau gets much more dispersed at increasing time horizons,
while betau stays more or less stable

weirdly enough, alphau + betau seems quite stable as well

Q_st_u 
=#
for idx_m in 1:6
# idx_m = 2
    fig,axs = subplots(3,4,figsize = (FIGSIZE_S[2] * 4, 3*FIGSIZE_S[1]))
    # t = 3000
    df_aggreg_g = groupby(df_aggreg_t3000_set2,:m,sort=true)
    axs[1,1].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].alphau_mean)
    axs[1,1].set_ylabel("alphau")
    axs[1,2].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].betau_mean)
    axs[1,2].set_ylabel("betau")
    axs[1,3].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].betau_mean .+ df_aggreg_g[idx_m].alphau_mean)
    axs[1,3].set_ylabel("alphau+betau")
    axs[1,4].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].betau_mean  ./ (df_aggreg_g[idx_m].betau_mean .+ df_aggreg_g[idx_m].alphau_mean))
    axs[1,4].set_ylabel("Q ST u mean")
    axs[1,4].set_title("t=3000")

    # t = 2000
    df_aggreg_g = groupby(df_aggreg_t2000_set2,:m,sort=true)
    axs[2,1].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].alphau_mean)
    axs[2,1].set_ylabel("alphau")
    axs[2,2].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].betau_mean)
    axs[2,2].set_ylabel("betau")
    axs[2,3].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].betau_mean .+ df_aggreg_g[idx_m].alphau_mean)
    axs[2,3].set_ylabel("alphau+betau")
    axs[2,4].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].betau_mean  ./ (df_aggreg_g[idx_m].betau_mean .+ df_aggreg_g[idx_m].alphau_mean))
    axs[2,4].set_ylabel("Q ST u mean")
    axs[2,4].set_title("t=2000")

    # t = 1000
    df_aggreg_g = groupby(df_aggreg_t1000_set2,:m,sort=true)
    axs[3,1].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].alphau_mean)
    axs[3,1].set_ylabel("alphau")
    axs[3,2].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].betau_mean)
    axs[3,2].set_ylabel("betau")
    axs[3,3].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].betau_mean .+ df_aggreg_g[idx_m].alphau_mean)
    axs[3,3].set_ylabel("alphau+betau")
    axs[3,4].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].betau_mean  ./ (df_aggreg_g[idx_m].betau_mean .+ df_aggreg_g[idx_m].alphau_mean))
    axs[3,4].set_ylabel("Q ST u mean")
    axs[3,4].set_title("t=1000")

    fig.suptitle("m = $(@sprintf("%1.2f",df_aggreg_g[idx_m].m[1]))")
    fig.savefig("diagnosis_time_Q_ST_u_m_$(@sprintf("%1.2f",df_aggreg_g[idx_m].m[1])).png", dpi = 500)
    gcf()
end

## super imposing to see evolution over time
#= conclusions
- the slope of alphau and betau appears steeper at large horizon times
- but the resulting slope of Q_ST_u appears less steep at large horizon times
- This could be due to the fact that at large horizon times, 

FINAL CONCLUSION:
what happens is that for low migration regimes, \beta_u is increasing at a much higher pace than \alpha (in fact,
    it could be that alpha stays constant). At large horizon time, the ratio $Q_ST_u$ therfore tends to 1 for all graphs,
    which of course reduces the explainability of r_\theta

We conclude that this would probably not arise if we introduce some sort of stabilising selection, which would allow betau 
    to have a steady state. 
    You could run simulations with t = 1000 and t = 2000 to verify this claim.

=#
idx_m = 4
fig,axs = subplots(1,4,figsize = (FIGSIZE_S[2] * 4, 1*FIGSIZE_S[1]))
# t = 3000
df_aggreg_g = groupby(df_aggreg_t3000_set2,:m,sort=true)
axs[1].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].alphau_mean, label = "t=3000")
axs[1].set_ylabel("alphau")
axs[2].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].betau_mean)
axs[2].set_ylabel("betau")
axs[3].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].betau_mean .+ df_aggreg_g[idx_m].alphau_mean)
axs[3].set_ylabel("alphau+betau")
axs[4].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].betau_mean  ./ (df_aggreg_g[idx_m].betau_mean .+ df_aggreg_g[idx_m].alphau_mean))
axs[4].set_ylabel("Q ST u mean")

# t = 2000
df_aggreg_g = groupby(df_aggreg_t2000_set2,:m,sort=true)
axs[1].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].alphau_mean, label = "t=2000")
axs[1].set_ylabel("alphau")
axs[2].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].betau_mean)
axs[2].set_ylabel("betau")
axs[3].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].betau_mean .+ df_aggreg_g[idx_m].alphau_mean)
axs[3].set_ylabel("alphau+betau")
axs[4].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].betau_mean  ./ (df_aggreg_g[idx_m].betau_mean .+ df_aggreg_g[idx_m].alphau_mean))
axs[4].set_ylabel("Q ST u mean")

# t = 1000
df_aggreg_g = groupby(df_aggreg_t1000_set2,:m,sort=true)
axs[1].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].alphau_mean, label = "t=1000")
axs[1].set_ylabel("alphau")
axs[2].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].betau_mean)
axs[2].set_ylabel("betau")
axs[3].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].betau_mean .+ df_aggreg_g[idx_m].alphau_mean)
axs[3].set_ylabel("alphau+betau")
axs[4].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].betau_mean  ./ (df_aggreg_g[idx_m].betau_mean .+ df_aggreg_g[idx_m].alphau_mean))
axs[4].set_ylabel("Q ST u mean")

fig.legend()
fig.suptitle("m = $(df_aggreg_g[idx_m].m[1])")
gcf()


# superimposing the relative variability 
idx_m = 1
fig,axs = subplots(1,4,figsize = (FIGSIZE_S[2] * 4, 1*FIGSIZE_S[1]))
# t = 3000
df_aggreg_g = groupby(df_aggreg_t3000_set2,:m,sort=true)
axs[1].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].alphau_std ./ df_aggreg_g[idx_m].alphau_mean, label = "t=3000")
axs[1].set_ylabel("alphau rel std")
axs[2].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].betau_std ./ df_aggreg_g[idx_m].betau_mean)
axs[2].set_ylabel("betau rel std")
axs[3].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].betau_std .+ df_aggreg_g[idx_m].alphau_std ./ (df_aggreg_g[idx_m].betau_mean .+ df_aggreg_g[idx_m].alphau_mean))
axs[3].set_ylabel("alphau+betau rel std")
axs[4].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].Q_ST_u_std  ./ df_aggreg_g[idx_m].Q_ST_u_mean)
axs[4].set_ylabel("Q ST u rel std")

# t = 2000
df_aggreg_g = groupby(df_aggreg_t2000_set2,:m,sort=true)
axs[1].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].alphau_std ./ df_aggreg_g[idx_m].alphau_mean, label = "t=2000")
axs[1].set_ylabel("alphau rel std")
axs[2].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].betau_std ./ df_aggreg_g[idx_m].betau_mean)
axs[2].set_ylabel("betau rel std")
axs[3].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].betau_std .+ df_aggreg_g[idx_m].alphau_std ./ (df_aggreg_g[idx_m].betau_mean .+ df_aggreg_g[idx_m].alphau_mean))
axs[3].set_ylabel("alphau+betau rel std")
axs[4].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].Q_ST_u_std  ./ df_aggreg_g[idx_m].Q_ST_u_mean)
axs[4].set_ylabel("Q ST u rel std")

# t = 1000
df_aggreg_g = groupby(df_aggreg_t1000_set2,:m,sort=true)
axs[1].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].alphau_std ./ df_aggreg_g[idx_m].alphau_mean, label = "t=1000")
axs[1].set_ylabel("alphau rel std")
axs[2].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].betau_std ./ df_aggreg_g[idx_m].betau_mean)
axs[2].set_ylabel("betau rel std")
axs[3].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].betau_std .+ df_aggreg_g[idx_m].alphau_std ./ (df_aggreg_g[idx_m].betau_mean .+ df_aggreg_g[idx_m].alphau_mean))
axs[3].set_ylabel("alphau+betau rel std")
axs[4].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].Q_ST_u_std  ./ df_aggreg_g[idx_m].Q_ST_u_mean)
axs[4].set_ylabel("Q ST u rel std")

fig.legend()
fig.suptitle("m = $(df_aggreg_g[idx_m].m[1])")
gcf()

# plotting the variability against r_\theta
idx_m = 6
fig,axs = subplots(3,2,figsize = (FIGSIZE_S[2] * 2.1, 3*FIGSIZE_S[1]))
# t = 3000
df_aggreg_g = groupby(df_aggreg_t3000_set2,:m,sort=true)
axs[1,1].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].alphau_std ./  df_aggreg_g[idx_m].betau_mean)
axs[1,1].set_ylabel("variability alphau")
axs[1,2].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].betau_std ./  df_aggreg_g[idx_m].betau_mean)
axs[1,2].set_ylabel("variability betau")
# t = 2000
df_aggreg_g = groupby(df_aggreg_t2000_set2,:m,sort=true)
axs[2,1].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].alphau_std ./  df_aggreg_g[idx_m].betau_mean)
axs[2,1].set_ylabel("variability alphau")
axs[2,2].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].betau_std ./  df_aggreg_g[idx_m].betau_mean)
axs[2,2].set_ylabel("variability betau")
# t = 1000
df_aggreg_g = groupby(df_aggreg_t1000_set2,:m,sort=true)
axs[3,1].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].alphau_std ./  df_aggreg_g[idx_m].betau_mean)
axs[3,1].set_ylabel("variability alphau")
axs[3,2].scatter(df_aggreg_g[idx_m].rθ, df_aggreg_g[idx_m].betau_std ./  df_aggreg_g[idx_m].betau_mean)
axs[3,2].set_ylabel("variability betau")
fig.suptitle("m = $(df_aggreg_g[idx_m].m[1])")
gcf()


# testing residuals
idx_m = 5
fig,axs = subplots(3,2,figsize = (FIGSIZE_S[2] * 2.1, 3*FIGSIZE_S[1]))
# t = 3000
df_aggreg_g = groupby(df_aggreg_t3000_set2,:m,sort=true)
df_temp = DataFrame(df_aggreg_g[idx_m][:,["Q_ST_u_mean","m","betau_mean","alphau_mean","N_mean", "sqrtk","cl", "rθ"]]); [df_temp[!,n] = df_temp[:,n] .|> Float64 for n in names(df_temp)]
mylmres = lm(@formula(alphau_mean ~ sqrtk + cl + N_mean), df_temp)
df_temp[!,"alphau_mean_residuals"] = residuals(mylmres)

axs[1,1].scatter(df_temp.rθ, df_temp.alphau_mean_residuals)
axs[1,1].set_ylabel("alphau mean residuals")
axs[1,2].scatter(df_temp.rθ, df_temp.Q_ST_u_mean)
axs[1,2].set_ylabel("Q ST u mean")
# t = 2000
df_aggreg_g = groupby(df_aggreg_t2000_set2,:m,sort=true)
df_temp = DataFrame(df_aggreg_g[idx_m][:,["Q_ST_u_mean","m","betau_mean","alphau_mean","N_mean", "sqrtk","cl", "rθ"]]); [df_temp[!,n] = df_temp[:,n] .|> Float64 for n in names(df_temp)]
mylmres = lm(@formula(alphau_mean ~ sqrtk + cl + N_mean), df_temp)
df_temp[!,"alphau_mean_residuals"] = residuals(mylmres)
axs[2,1].scatter(df_temp.rθ, df_temp.alphau_mean_residuals)
axs[2,1].set_ylabel("alphau mean residuals")
axs[2,2].scatter(df_temp.rθ, df_temp.Q_ST_u_mean)
axs[2,2].set_ylabel("Q ST u mean")
# t = 1000
df_aggreg_g = groupby(df_aggreg_t1000_set2,:m,sort=true)
df_temp = DataFrame(df_aggreg_g[idx_m][:,["Q_ST_u_mean","m","betau_mean","alphau_mean","N_mean", "sqrtk","cl", "rθ"]]); [df_temp[!,n] = df_temp[:,n] .|> Float64 for n in names(df_temp)]
mylmres = lm(@formula(alphau_mean ~ sqrtk + cl + N_mean), df_temp)
df_temp[!,"alphau_mean_residuals"] = residuals(mylmres)
axs[3,1].scatter(df_temp.rθ, df_temp.alphau_mean_residuals)
axs[3,1].set_ylabel("alphau mean residuals")
axs[3,2].scatter(df_temp.rθ, df_temp.Q_ST_u_mean)
axs[3,2].set_ylabel("Q ST u mean")
fig.suptitle("m = $(df_temp.m[1])")
gcf()



\begin{tabular}{lrrrrr}
\toprule
               & \multicolumn{1}{c}{0.01} & \multicolumn{1}{c}{0.05} & \multicolumn{1}{c}{0.10} & \multicolumn{1}{c}{0.50} & \multicolumn{1}{c}{1.00} \\ 
\cmidrule(lr){2-2} \cmidrule(lr){3-3} \cmidrule(lr){4-4} \cmidrule(lr){5-5} \cmidrule(lr){6-6} 
               &                                                    \multicolumn{5}{c}{$Q_{ST,u}$}                                                    \\ 
\cmidrule(lr){2-6} 
(Intercept)    &                    0.000 &                   -0.000 &                   -0.000 &                   -0.000 &                   -0.000 \\ 
               &                  (0.019) &                  (0.015) &                  (0.014) &                  (0.013) &                  (0.013) \\ 
$h_d$          &                -0.446*** &                -0.523*** &                -0.497*** &                -0.353*** &                -0.202*** \\ 
               &                  (0.021) &                  (0.016) &                  (0.016) &                  (0.015) &                  (0.014) \\ 
$cl$           &                 0.539*** &                 0.539*** &                 0.566*** &                 0.710*** &                 0.818*** \\ 
               &                  (0.021) &                  (0.016) &                  (0.016) &                  (0.015) &                  (0.014) \\ 
\midrule
Number of sim. &                      853 &                      853 &                      853 &                      853 &                      853 \\ 
$R^2$          &                    0.706 &                    0.819 &                    0.822 &                    0.856 &                    0.859 \\ 
\bottomrule
\end{tabular}

#= 
This scripts aims at numerically verifying diversification
conditions under trait dependency competition
=#

cd(@__DIR__)
name_sim = split(splitpath(@__FILE__)[end],".")[1]
using DiffEqOperators, LinearAlgebra, LightGraphs, Distributions
using DifferentialEquations,Random
using Printf
using UnPack,DataFrames,JLD2,Dates
import EvoId:gaussian
using IDEvol

## Parameters used
mu= 0.1
K1 = 1
σ_α = 0.5
M = 2;
dS = 0.02;
rS = 2.0;
σ_mu = 5e-2;
c = 1e0 / K1;
tend = 500
# tend = 1.5
tspan = (0.0,tend)

## Overwritten later on
m = 0.1
r_θ = -0.5
## rest of the simulation

S = collect(range(-rS,rS,step=dS)) #grid
N = length(S)
Δ_s = Array( σ_mu^2/2 *mu * CenteredDifference{1}(2, 2, dS, N))[:,2:end-1]
X = 1:M
soptim = [-0.5,0.5]
u0 = vcat([K1 .* pdf.(Normal(so,σ_mu),S') for so in soptim]...)


p_default = Dict{String,Any}()
@pack! p_default = M,S,dS,N,Δ_s,σ_mu,X,soptim,m,r_θ,σ_α

B(x,s,soptim) = 1e0 - (s - soptim[x])^2

# this function is called to evaluate the convolution of local u and alpha evaluated at S[i]
function int_u(u::T,p::Dict,x) where T <: AbstractArray
        @unpack N,S,dS,σ_α = p
        C = 0.5 * (u[1] * exp(-0.5 * (x - S[1])^2 / σ_α^2) + u[N] * exp(-0.5 * (x - S[end])^2 / σ_α^2))
        C += sum(u[2:N-1] .* exp.(-0.5 * (x .- S[2:N-1]).^2 / σ_α^2))
        return C*dS
end

# non linear term, i.e. the logistic summand of the IDE
function f(du,u,p,t)
        @unpack N,M,S,dS,X,soptim,m,r_θ= p
        u[u[:,:] .< eps()] .= 0
        C_1 = int_u.(Ref(@view u[1,:]), Ref(p), S)
        C_2 = int_u.(Ref(@view u[2,:]), Ref(p), S)
        B1 = B.(X[1],S,Ref(soptim))
        B2 = B.(X[2],S,Ref(soptim))
        du[1,:] .= u[1,:] .* (B1 * (1-m) .- C_1 / K1  ) +   Δ_s * (u[1,:] .* B1)  + 0.5 * m * ((1 + r_θ) * u[1,:] .* B1 + (1 - r_θ) * u[2,:] .* B2)
        du[2,:] .= u[2,:] .* (B2 * (1-m) .-  C_2 / K1 ) +   Δ_s * (u[2,:] .* B2)  + 0.5 * m * ((1 + r_θ) * u[2,:] .* B2 + (1 - r_θ) * u[1,:] .* B1)
        # du .= du .* .!( u[:,:] .< eps() .* du .< 0.)
        return du
end



prob = ODEProblem(f,u0,tspan,p_default)
@time sol = solve(prob)

uend = copy(sol.u[end])
println("beta diversity is :", βdiv(uend,p_default))

using PyPlot
plt.plot(S, uend[1,:]); gcf()

df = DataFrame("uend" => [], "rθ" => Float64[], "m" => Float64[], "N" => Float64[], "β" => Float64[], "s1" => Float64[])
ms = range(0., .7, length=15)
rts = [-0.5, 0.5]
for m in ms, rt in rts
    p = copy(p_default)
    p["m"] = m
    p["r_θ"] = rt
    prob = ODEProblem(f,u0,tspan,p)
    println("m = ", m)
    @time sol = solve(prob,alg=Tsit5())
    uend = copy(sol.u[end])
    # s = get_types(uend)[1]
    # push!(df,(uend, rt, m, popsize(uend,p), βdiv(uend,p), s))

    push!(df,(uend, rt, m, popsize(uend,p), βdiv(uend,p), 0.))
end
using CSV
CSV.write("data_PDE_distrib.csv", df)
#= 
This script works with EvoId v5.0.0

Here we explore compute two and thre variate linear models
to explore effect of graph features on β_u
=# 

cd(@__DIR__)
using LightGraphs
using DataFrames
using CSV
using GLM
using Statistics
using JLD2
using Printf
using RegressionTables
using StatsBase

name_sim = "setting_2_mu_01_realistic_graphs_higher_temp_range"
@load "$(name_sim)_aggreg.jld2" df_aggreg
@load "../../../../graphs_utils/real_graphs/graph_prop_realistic_graphs.jld2" graphs_df

println("we found ", count(isnan.(df_aggreg.betas_mean)), " NaN for betas_mean")
for var in ["var._rich_club_nx","degree_correlation","mean_neighb._deg.","var._neighb._deg.","var._deg._distrib.","kk","betas_mean"]
        filter!(x -> !(ismissing(x[var]) || isnothing(x[var]) || isnan(x[var])), df_aggreg)
end

metrics = ["var._local_clustering_coefficient", "mean_betweenness_centrality", "var._betweenness_centrality", "mean_edge_betweeness_centrality_nx", "var._edge_betweeness_centrality_nx", "mean_eigenvector_centrality", "var._eigenvector_centrality", "mean_rich_club_nx", "var._rich_club_nx", "mean_closeness_centrality", "var._closeness_centrality", "density", "degree_var.", "degree_correlation", "cl", "mean_neighb._deg.", "var._neighb._deg.", "smax", "s_elasticity", "modules", "mean_deg._distrib.", "var._deg._distrib.", "diameter", "graph_energy", "algebraic_connectivity", "kk", "sqrtk", "sqrtk_inv", "heat_hetero", "rθ_temp"]
# metrics = [metrics;"rθ_temp"]
# we remove mean betweenness centrality as its correlation is one with cl
metrics = vcat(metrics[.!(metrics .∈ Ref([
                                        "mean_betweenness_centrality",
                                        "heat_hetero",
                                        "sqrtk_inv",
                                        "kk"]))])
expl_var = vcat(metrics,:betas_mean)
##############################
########## 2 variate #########
##############################
expl_var = Symbol.(expl_var)
df_lr = DataFrame(pred1 = String[], pred2 = String[], R2 = Float64[], aicc = Float64[], lr = [], m = Float64[],idx_model=Int[])
for m in sort!(unique(df_aggreg.m))
        i = 1
        df_totrain_R = Float64.(df_aggreg[df_aggreg.m .== m, expl_var])
        println("We have ", count(isnan.(Matrix(df_totrain_R))), " nan left")
        for _e in expl_var
                df_totrain_R[:,_e] = (df_totrain_R[:,_e] .- mean(df_totrain_R[:,_e])) ./ std(df_totrain_R[:,_e])
        end
        _metrics = copy(metrics)
        for pred1 in metrics
                popfirst!(_metrics)
                for pred2 in _metrics
                        try
                                _flm = Term(:betas_mean) ~ Term(Symbol(pred1)) + Term(Symbol(pred2))
                                linearRegressor = lm(_flm,df_totrain_R)
                                push!(df_lr,(pred1,pred2,r2(linearRegressor),aicc(linearRegressor),linearRegressor,m,i))
                                i+=1
                        catch e
                                println("problem with ", pred1, " and ", pred2)
                                println(e)
                        end
                end
        end
end
df_lr_g = groupby(df_lr,:idx_model)
models_aggreg = combine(df_lr_g,:R2=>mean,:aicc=>mean,:idx_model => mean)
sort!(models_aggreg,:R2_mean,rev=true)
println("Best fit models are")
for i in 1:10
        _model = df_lr[df_lr.idx_model .== models_aggreg.idx_model_mean[i],:]
        println(_model.pred1[1], " and ", _model.pred2[1], ", r2 = ", mean(_model.R2))
end


repl_dict = Dict("_" => " ")
function transform(s, repl_dict=repl_dict)
        for (old, new) in repl_dict
            s = replace.(s, Ref(old => new))
        end
        s
end

for m in sort!(unique(df_aggreg.m))
        println("latex_print m= $m")
        _lr = []
        for i in 1:3
                push!( _lr, first(df_lr[ (df_lr.idx_model .== models_aggreg.idx_model_mean[i]) .* (df_lr.m .== m ), :lr]))
        end
        println("m = $m")
        regtable(_lr...; renderSettings = latexOutput("adapt_stat_m=$(m)_twovariate.txt"),print_estimator_section=false,regression_statistics=[:r2])
        # open(, "w") do io
        #         write(io, tex)
        # end;
end

##########################
######## 3 variate #######
##########################
# similar as above with three variates, except that we constrain the predictors to be exactly rθ_temp, cl, and sqrtk
expl_var = Symbol.(expl_var)
df_lr = DataFrame(pred1 = String[], pred2 = String[],pred3 = String[], R2 = Float64[], aicc = Float64[], lr = [], m = Float64[],idx_model=Int[])
for m in sort!(unique(df_aggreg.m))
        i = 1
        df_totrain_R = Float64.(df_aggreg[df_aggreg.m .== m, expl_var])
        println("We have ", count(isnan.(Matrix(df_totrain_R))), " nan left")
        for _e in expl_var
                df_totrain_R[:,_e] = (df_totrain_R[:,_e] .- mean(df_totrain_R[:,_e])) ./ std(df_totrain_R[:,_e])
        end
        pred1 = "rθ_temp"; pred2 = "cl"; pred3 = "sqrtk";
        try
                _flm = Term(:betas_mean) ~ Term(Symbol(pred1)) + Term(Symbol(pred2)) + Term(Symbol(pred2)) + + Term(Symbol(pred3))
                linearRegressor = lm(_flm,df_totrain_R)
                push!(df_lr,(pred1,pred2,pred3,r2(linearRegressor),aicc(linearRegressor),linearRegressor,m,i))
                i+=1
        catch e
                println("problem with ", pred1, " and ", pred2)
                println(e)
        end
end
df_lr_g = groupby(df_lr,:idx_model)
models_aggreg = combine(df_lr_g,:R2=>mean,:aicc=>mean,:idx_model => mean)
sort!(models_aggreg,:R2_mean,rev=true)
println(df_lr)


repl_dict = Dict("_" => " ")
function transform(s, repl_dict=repl_dict)
        for (old, new) in repl_dict
            s = replace.(s, Ref(old => new))
        end
        s
end

for m in sort!(unique(df_aggreg.m))
        println("latex_print m= $m")
        _lr = []
        for i in [1]
                push!( _lr, first(df_lr[ (df_lr.idx_model .== models_aggreg.idx_model_mean[i]) .* (df_lr.m .== m ), :lr]))
        end
        regtable(_lr...; renderSettings = latexOutput("adapt_stat_m=$(m)_threevariate.txt"),print_estimator_section=false,regression_statistics=[:r2])
        # open(, "w") do io
        #         write(io, tex)
        # end;
end
cd(@__DIR__)
name_sim = "setting_2_mu_01_M=7_hetero_2_trait_comp_exp"
tend = 500.
using EvoId,JLD2
using DataFrames
using Glob
using LightGraphs

df_arrays = []
flist = glob("$name_sim/*.jld2")
for f in flist
    try
        push!(df_arrays,jldopen(f,"r")["df"]);
    catch e
        println(e)
    end
end

# checking if all .jld2 with different seeds have same number of simulations
all([ s == size(df_arrays[1], 1) for s in size.(df_arrays, 1)])

# variables that do not need statistics
vars_notstat = ["m", "μ", "D", "graph", "rθ", "σ_α"]
#variables needing statistics
vars = ["N", "betas", "betau"]
df_aggreg = DataFrame(vcat(vars_notstat .=> [[] for _ in 1:length(vars_notstat)],
                    vars.*"_mean" .=> [Float64[] for _ in 1:length(vars)],
                    vars.*"_std" .=> [Float64[] for _ in 1:length(vars)]))

#filling df_aggreg
using ProgressMeter
@showprogress for r in eachrow(df_arrays[1])
    @assert r.tend > tend
    ltemp = [r[vars] |> Vector]
    for j in 2:length(df_arrays)
        for r2 in eachrow(df_arrays[j])
            # checking if rows match with first df
            if all(Vector(r[vars_notstat]) .== Vector(r2[vars_notstat]))
                @assert r2.tend > tend
                push!(ltemp, r2[vars] |> Vector)
                break
            end
        end
    end
    # println("matched row $(length(ltemp)) times")
    _stats = [r[vars_notstat] |> Vector; mean(ltemp); std(ltemp)]
    push!(df_aggreg, _stats)
end

# filtering nan etcs
for v in vars.*"_mean"
    println("we found ", count(isnan.(df_aggreg[:,v])), " NaN for $v")
    filter!(v => x -> !(ismissing(x) || isnothing(x) || isnan(x)), df_aggreg)
end

#################################
### matching df with graphs_df ##
#################################
@load "../../../graphs_utils/M=7/graph_prop_M=7.jld2" graphs_df
println("matching df and graphs_df")
metrics =  names(graphs_df[:,2:end])



for _n in metrics
    df_aggreg[!,_n] = zeros(size(df_aggreg,1))
end

global i = 0
for r in eachrow(df_aggreg)
    for _i in 1:size(graphs_df,1)
        if graphs_df.graph[_i] == r.graph
            r[metrics] = graphs_df[_i,2:end]
            global i+= 1
            break
        end
    end
end
println("df matched $i / $(size(df_aggreg,1))")

@save "$name_sim/$(name_sim)_aggreg.jld2" df_aggreg
using CSV
CSV.write("$name_sim/$(name_sim)_aggreg.csv", df_aggreg[:,Not("graph")])
cd(@__DIR__)
df_aggreg = true
extract_div = "ExtractDiversity_neutr_final.jl"
using EvoId,JLD2
using DataFrames
using Glob
using LightGraphs
using LaTeXStrings
# Extracting basic features of simulations
# We use the same pipeline as originally with bashscript
# yet this could be changed by building an appropriate functions for out of ExtractDiversity_____.jl

rlist = glob("*/")
for r in rlist
    cp(extract_div,joinpath(r,extract_div),force=true)
    [deleteat!(Base.ARGS,i) for i in 1:length(Base.ARGS)]
    push!(Base.ARGS,"results_$(r[1:end-1]).jld2")
    include(joinpath(r,extract_div))
    cd(@__DIR__)
    mv(joinpath(r,"results_$(r[1:end-1]).jld2"),joinpath(@__DIR__,"results_$(r[1:end-1]).jld2"),force=true)
end

df_arrays = []
flist = glob("results*.jld2",@__DIR__);
for f in flist
    try
        push!(df_arrays,jldopen(f,"r")["df_explo_all"]);
    catch e
        println(e)
    end
end
df = copy(df_arrays[1])
dfvar = copy(df)
sp = sortperm(size.(df_arrays,1))
for i in 1:size(df_arrays[sp[1]],1)
    ltemp = DataFrame(df_arrays[sp[1]][i,:])
    for j in sp[2:end]
        for k in 1:size(df_arrays[j],1)
            ltemp2 = DataFrame(df_arrays[j][k,:])
            if ltemp.graph[1] == ltemp2.graph[1] && ltemp.m[1] == ltemp2.m[1] && isapprox(ltemp.t[1],ltemp2.t[1],atol = 0.1)
                ltemp = vcat(ltemp,ltemp2)
            end
        end
    end
    println("matched row $i, $(size(ltemp,1)) times")
    for idx in [:alphau,:betau,:gammau,:N,:m,:D2]
        df[i,idx] = mean(ltemp[idx])
        dfvar[i,idx] = std(ltemp[idx])
    end
    df[i,[:graph,:NE]] = dfvar[i,[:graph,:NE]] = ltemp[1,[:graph,:NE]]
end
dfvar[:,[:m,:D2,:t,:graph]] .= df[:,[:m,:D2,:t,:graph]];

filter!(row -> row.t >= 500., df)
filter!(row -> row.t >= 500., dfvar)

println("Have have kept ", size(df,1), " / ", size(df_arrays[1],1), " original simulations.")

@save "df_aggreg.jld2" df dfvar

#= 
This script works with EvoId v5.0.0

Here we explore compute multivariate linear models
with all metrics
to explore effect of graph features on β_u
=# 
cd(@__DIR__)
using LightGraphs
using DataFrames
using CSV
using GLM
using Statistics
using MLBase
using JLD2
using Printf
using RegressionTables


name_sim = "setting_1_mu_01_M=8"
@load "$(name_sim)_aggreg.jld2" df_aggreg
@load "../../../../graphs_utils/M=8/graph_prop_M=8.jld2" graphs_df

println("we found ", count(isnan.(df_aggreg.betau_mean)), " NaN for betau_mean")
for var in ["var._rich_club_nx","degree_correlation","mean_neighb._deg.","var._neighb._deg.","var._deg._distrib.","kk","betau_mean"]
        filter!(x -> !(ismissing(x[var]) || isnothing(x[var]) || isnan(x[var])), df_aggreg)
end

metrics = names(graphs_df)[2:end]
# we remove mean betweenness centrality as its correlation is one with cl
metrics = vcat(metrics[.!(metrics .∈ Ref([
                                        "mean_betweenness_centrality",
                                        "heat_hetero",
                                        "sqrtk_inv",
                                        "kk"]))])
expl_var = vcat(metrics,:betau_mean)
##############################
########## all variate #########
##############################
expl_var = Symbol.(expl_var)
df_lr = DataFrame(R2 = Float64[], aicc = Float64[], lr = [], m = Float64[])
for m in sort!(unique(df_aggreg.m))
        i = 1
        df_totrain_R = Float64.(df_aggreg[df_aggreg.m .== m, expl_var])
        println("We have ", count(isnan.(Matrix(df_totrain_R))), " nan left")
        for _e in expl_var
                df_totrain_R[:,_e] = (df_totrain_R[:,_e] .- mean(df_totrain_R[:,_e])) ./ std(df_totrain_R[:,_e])
        end
        
        _flm = Term(:betau_mean) ~ sum(Term.(Symbol.(metrics)))
        linearRegressor = lm(_flm,df_totrain_R)
        push!(df_lr,(r2(linearRegressor),aicc(linearRegressor),linearRegressor,m))
end
println(df_lr.lr[3])
println(df_lr.lr[4])

##############################
########## including m #######
##############################
# metrics = vcat(metrics,"m")
expl_var = vcat(metrics,:betau_mean) .|> Symbol
df_temp = df_aggreg[:,expl_var] .|> Float64
_flm = Term(:betau_mean) ~ sum(Term.(Symbol.(metrics)))
for _e in expl_var
        df_temp[:,_e] = (df_temp[:,_e] .- mean(df_temp[:,_e])) ./ std(df_temp[:,_e])
end
linearRegressor = lm(_flm,df_temp)


# One should try using R to obtain VIF and discard those with score higher than 5
# or one could also use the removeColinearity function to group 
df_aggreg_g = groupby(df_aggreg,:m, sort=true)
df_temp = df_aggreg_g[1]
using RCall
R"""
library(regclass)
df_temp <-$df_temp
M <- lm(betau_mean ~ ., data = df_temp)
myvif <- sort(VIF(M))
print(myvif)
newM <- lm(betau_mean ~ . - var._eigenvector_centrality - mean_eigenvector_centrality - mean_closeness_centrality - cl, data = df_temp)
print(summary(newM))
myvif <- sort(VIF(newM))
print(myvif)
"""
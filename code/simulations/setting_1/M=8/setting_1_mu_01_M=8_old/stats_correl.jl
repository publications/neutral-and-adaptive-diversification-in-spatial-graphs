cd(@__DIR__)
using LightGraphs
using DataFrames
using CSV
using GLM
using Statistics
using MLBase
using JLD2
using Printf
include("../../../../../figures/format.jl")


name_sim = "setting_1_mu_01_M=8"
@load "$(name_sim)_aggreg.jld2" df_aggreg
@load "../../../../graphs_utils/M=8/graph_prop_M=8.jld2" graphs_df

println("we found ", count(isnan.(df_aggreg.betau_mean)), " NaN for betau_mean")
for var in ["var._rich_club_nx","degree_correlation","mean_neighb._deg.","var._neighb._deg.","var._deg._distrib.","kk","betau_mean"]
        filter!(x -> !(ismissing(x[var]) || isnothing(x[var]) || isnan(x[var])), df_aggreg)
end

metrics = names(graphs_df)[2:end]
# we remove mean betweenness centrality as its correlation is one with cl
metrics = vcat(metrics[.!(metrics .∈ Ref([
                                        "heat_hetero",
                                        "sqrtk_inv",
                                        "kk"]))])
expl_var = vcat(metrics,:betau_mean)

expl_var = Symbol.(expl_var)
df_lr = DataFrame(pred1 = String[], cor = Float64[], m = Float64[])
for m in unique(df_aggreg.m)
        df_totrain_R = Float64.(df_aggreg[df_aggreg.m .== m, expl_var])
        println(size(df_totrain_R,1), "simulations for m = ", m)
        println("We have ", count(isnan.(Matrix(df_totrain_R))), " nan left")
        for pred1 in metrics
                push!(df_lr, (pred1, cor(df_totrain_R.betau_mean,df_totrain_R[:,pred1]), m))
        end
end

df_lr_g = groupby(df_lr,:pred1)
models_aggreg = combine(df_lr_g,:cor=>mean,:cor=>std,:cor => abs∘mean)
sort!(models_aggreg,:cor_abs_mean,rev=true)
println("Best metrics  are")
# [ println(models_aggreg.pred1[i], ", cor = ", models_aggreg.mean cor.[i]) for i in 1:34 ]

## Latex correlation table
df_latex = copy(models_aggreg)
df_m_g = groupby(df_lr,:m)

for _df in df_m_g
    df_latex = innerjoin(df_latex,_df[:,[:pred1,:cor]], on = :pred1)
    rename!(df_latex,Dict("cor" => string("m = ",@sprintf("%1.2f",_df.m[1]))))
end
for i in 1:size(df_latex,1)
        df_latex[i,:pred1] = replace(df_latex[i,:pred1],"nx"=>"")
        df_latex[i,:pred1] = replace(df_latex[i,:pred1],"_" => " ")
        # df_latex[i,:pred1] = replace(df_latex[i,:pred1],"_"=>" ")
        # df_latex[i,:pred1] = LaTeXString(df_latex[i,:pred1])
end
for i in 1:size(df_aggreg,2)
        rename!(df_aggreg, Dict(names(df_aggreg)[i] => replace(names(df_aggreg)[i],"_" => " ")))
        rename!(df_aggreg, Dict(names(df_aggreg)[i] => replace(names(df_aggreg)[i],"nx"=>"")))
end
rename!(df_aggreg, Dict("sqrtk"=>L"\nicefrac{\left\langle \sqrt{k}\right\rangle^{2}}{<k>}"))
df_latex[df_latex.pred1 .== "sqrtk",:pred1] .= L"\nicefrac{\left\langle \sqrt{k}\right\rangle^{2}}{<k>}"
sort!(df_latex,"cor_abs_mean", rev=true)

if true
        using Latexify
        tab = latexify(df_latex[:,["pred1","m = 0.01","m = 0.05","m = 0.10","m = 0.50","cor_mean","cor_std"]],env=:tabular,fmt="%.2f", latex=false) #|> String
        io = open("correlation_table_neutr.tex", "w")
        write(io,tab);
        close(io)
end

if true
        using PyCall, PyPlot
        sns = pyimport("seaborn")
        pd = pyimport("pandas")
        np = pyimport("numpy")

        df_cor = df_latex[:,:cor_mean]
        pushfirst!(df_cor,1.0)
        arr_cor = zeros(length(df_cor),length(df_cor))
        arr_cor[1,:] = arr_cor[:,1] = df_cor
        for (i,pred1) in enumerate(df_latex.pred1)
                for (j,pred2) in enumerate(df_latex.pred1)
                        arr_cor[i+1,j+1] = arr_cor[j+1,i+1] = cor(df_aggreg[:,pred1],df_aggreg[:,pred2])
                end
        end

        col_arr = vcat(L"\beta_u",df_latex.pred1)
        df_pd = pd.DataFrame(arr_cor,columns = col_arr,index =col_arr )
        mask = np.triu(np.ones_like(df_pd, dtype="bool"))

        f, ax = plt.subplots(
                        figsize=(FIGSIZE_L[1],FIGSIZE_L[1])
                        )
        # Generate a custom diverging colormap
        cmap = sns.diverging_palette(230, 20, as_cmap=true)
        # Draw the heatmap with the mask and correct aspect ratio
        sns.heatmap(df_pd,
                # mask=mask,
                cmap=cmap,
                # vmin = -1.,
                # vmax = 1.,
                # center=0,
                # xticks = myticks,
                square=true,
                linewidths=.5,
                cbar_kws=Dict("shrink"=> .5),
                # annot=true,
                # xticklabels=false,
                fmt = ".1f")
        # ax.tick_params(labelsize=18)
        # ax.set_title("m = $(m_toplot)",pad=-50,y = 1.0)
        plt.tight_layout()
        plt.savefig("plot_cor_neutr_betau_mean_lowmut.pdf",
                dpi=1200,
                bbox_inches = "tight",)
        gcf()
end

cd(@__DIR__)
using LightGraphs
using DataFrames
using CSV
using GLM
using Statistics
using MLBase
using JLD2
using Printf
using RegressionTables


name_sim = "setting_1_mu_01_M=8"
@load "$(name_sim)_aggreg.jld2" df_aggreg
@load "../../../../graphs_utils/M=8/graph_prop_M=8.jld2" graphs_df

println("we found ", count(isnan.(df_aggreg.betau_mean)), " NaN for betau_mean")
for var in ["var._rich_club_nx","degree_correlation","mean_neighb._deg.","var._neighb._deg.","var._deg._distrib.","kk","betau_mean"]
        filter!(x -> !(ismissing(x[var]) || isnothing(x[var]) || isnan(x[var])), df_aggreg)
end

metrics = names(graphs_df)[2:end]
# we remove mean betweenness centrality as its correlation is one with cl
metrics = vcat(metrics[.!(metrics .∈ Ref([
                                        "mean_betweenness_centrality",
                                        "heat_hetero",
                                        "sqrtk_inv",
                                        "kk"]))])
expl_var = vcat(metrics,:betau_mean)

expl_var = Symbol.(expl_var)
df_lr = DataFrame(pred1 = String[], pred2 = String[], R2 = Float64[], aicc = Float64[], lr = [], m = Float64[],idx_model=Int[])
for m in sort!(unique(df_aggreg.m))
        i = 1
        df_totrain_R = Float64.(df_aggreg[df_aggreg.m .== m, expl_var])
        println("We have ", count(isnan.(Matrix(df_totrain_R))), " nan left")
        for _e in expl_var
                df_totrain_R[:,_e] = (df_totrain_R[:,_e] .- mean(df_totrain_R[:,_e])) ./ std(df_totrain_R[:,_e])
        end
        _metrics = copy(metrics)
        for pred1 in metrics
                popfirst!(_metrics)
                for pred2 in _metrics
                        try
                                _flm = Term(:betau_mean) ~ Term(Symbol(pred1)) + Term(Symbol(pred2))
                                linearRegressor = lm(_flm,df_totrain_R)
                                push!(df_lr,(pred1,pred2,r2(linearRegressor),aicc(linearRegressor),linearRegressor,m,i))
                                i+=1
                        catch e
                                println("problem with ", pred1, " and ", pred2)
                                println(e)
                        end
                end
        end
end
df_lr_g = groupby(df_lr,:idx_model)
models_aggreg = combine(df_lr_g,:R2=>mean,:aicc=>mean,:idx_model => mean)
sort!(models_aggreg,:R2_mean,rev=true)
println("Best fit models are")
for i in 1:10
        _model = df_lr[df_lr.idx_model .== models_aggreg.idx_model_mean[i],:]
        println(_model.pred1[1], " and ", _model.pred2[1], ", r2 = ", mean(_model.R2))
end


repl_dict = Dict("_" => " ")
function transform(s, repl_dict=repl_dict)
        for (old, new) in repl_dict
            s = replace.(s, Ref(old => new))
        end
        s
end

for m in sort!(unique(df_aggreg.m))
        println("latex_print m= $m")
        _lr = []
        for i in 1:3
                push!( _lr, first(df_lr[ (df_lr.idx_model .== models_aggreg.idx_model_mean[i]) .* (df_lr.m .== m ), :lr]))
        end
        println("m = $m")
        regtable(_lr...; renderSettings = latexOutput("neutr_stat_m=$(m).txt"),print_estimator_section=false,regression_statistics=[:r2])
        # open(, "w") do io
        #         write(io, tex)
        # end;
end

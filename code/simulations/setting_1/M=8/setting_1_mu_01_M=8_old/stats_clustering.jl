cd(@__DIR__)
using LightGraphs
using DataFrames
using CSV
using GLM
using Statistics
using MLBase
using JLD2
using Printf
include("../../../../../figures/format.jl")

M=8
name_sim = "setting_1_mu_01_M=$M"
@load "$(name_sim)_aggreg.jld2" df_aggreg
@load "../../../../graphs_utils/M=$M/graph_prop_M=$M.jld2" graphs_df

println("we found ", count(isnan.(df_aggreg.betau_mean)), " NaN for betau_mean")
for var in ["var._rich_club_nx","degree_correlation","mean_neighb._deg.","var._neighb._deg.","var._deg._distrib.","kk","betau_mean"]
        filter!(x -> !(ismissing(x[var]) || isnothing(x[var]) || isnan(x[var])), df_aggreg)
end

metrics = names(graphs_df)[2:end]
# we remove mean betweenness centrality as its correlation is one with cl
metrics = vcat(metrics[.!(metrics .∈ Ref([
                                        "mean_betweenness_centrality",
                                        "heat_hetero",
                                        "sqrtk_inv",
                                        "kk"]))])
# expl_var = vcat(metrics,:betau_mean)
expl_var = metrics

expl_var = Symbol.(expl_var)

######################################
##### working clustering with R ######
######################################
dist = 1 .- cor(df_aggreg[:,metrics] |> Array)

using RCall
R"""
dist_mat <- $(dist)
rownames(dist_mat) <- $metrics
colnames(dist_mat) <- $metrics
dist <- as.dist(dist_mat)
hc <- hclust(dist)
phc <- plot(hc)
"""


#  with pheatmap
R"""
library(pheatmap)
dist_df <- as.data.frame(dist_mat)
pheatmap(dist_df)
"""
#= 
This script works with EvoId v5.0.0

Here we explore compute multivariate linear models
with all metrics
to explore effect of graph features on β_u
=# 
cd(@__DIR__)
using LightGraphs
using DataFrames
using CSV
using GLM
using Statistics
using MLBase
using JLD2
using Printf
using RegressionTables


name_sim = "setting_1_mu_01_M=8"
@load "$(name_sim)_aggreg.jld2" df_aggreg
@load "../../../../graphs_utils/M=8/graph_prop_M=8.jld2" graphs_df

println("we found ", count(isnan.(df_aggreg.betau_mean)), " NaN for betau_mean")
for var in ["var._rich_club_nx","degree_correlation","mean_neighb._deg.","var._neighb._deg.","var._deg._distrib.","kk","betau_mean"]
        filter!(x -> !(ismissing(x[var]) || isnothing(x[var]) || isnan(x[var])), df_aggreg)
end


metrics = names(graphs_df)[2:end]
# we remove mean betweenness centrality as its correlation is one with cl
metrics = vcat(metrics[.!(metrics .∈ Ref([
                                        "mean_betweenness_centrality",
                                        "heat_hetero",
                                        "sqrtk_inv",
                                        "kk"]))])
expl_var = vcat(metrics, :betau_mean, :N_mean) .|> Symbol

df_aggreg_g = groupby(df_aggreg, :m, sort=true)
df_temp_m001 = df_aggreg_g[1][:,expl_var] .|> Float64
df_temp_m005 = df_aggreg_g[2][:,expl_var] .|> Float64
df_temp_m01 = df_aggreg_g[4][:,expl_var] .|> Float64
df_temp_m05 = df_aggreg_g[4][:,expl_var] .|> Float64
df_temp_m10 = df_aggreg_g[5][:,expl_var] .|> Float64

using RCall
R"""library(regclass)
df_temp <- as.data.frame(scale($df_temp_m05))
M1 <- lm(betau_mean ~ ., data = df_temp)
# print(summary(M1))
print(coef(summary(M1))[order(coef(summary(M1))[,1], decreasing = T),])
M2 <- lm(betau_mean ~ sqrtk + cl, data = df_temp)
print(summary(M2))
VIF(M2
)"""

#model selection with stepwise regression
R"""
library(MASS)
df_temp <- as.data.frame(scale($df_temp_m01))
# Fit the full model 
full.model <- lm(betau_mean ~., data = df_temp)
# Stepwise regression model
step.model <- stepAIC(full.model, direction = "both", 
                      trace = FALSE)
print(summary(step.model))
# print(coef(summary(step.model))[order(coef(summary(step.model))[,1], decreasing = T),])
print(VIF(step.model))
"""

# removing VIF automatically
R"""
df_temp <- as.data.frame(scale($df_temp_m01))
library(janitor)
vif_fun <- function(df){
             df <- janitor::clean_names(df)
             while(TRUE) {
                vifs <- VIF(lm(betau_mean ~. , data = df))
                if (max(vifs) < 10) {
                     break
                }
               highest <- c(names((which(vifs == max(vifs)))))
               df <- df[,-which(names(df) %in% highest)]

              }
            return(df)
              }
df_vif <- vif_fun(df_temp)
model_vif <-lm(betau_mean ~., data = df_vif)
print(summary(model_vif))
"""
df_vif = rcopy(R"df_vif")

## checking effect of h_d on betau_mean
using RCall
R"""
df_temp <- as.data.frame(scale($df_temp_m005))
M_N <- lm(betau_mean ~ N_mean, data = df_temp)
M_sqrtk <- lm(betau_mean ~ sqrtk, data = df_temp)
print(summary(M_N))
print(summary(M_sqrtk))

M2 <- lm(M_N$residuals ~ df_temp$sqrtk)
print(summary(M2))
"""